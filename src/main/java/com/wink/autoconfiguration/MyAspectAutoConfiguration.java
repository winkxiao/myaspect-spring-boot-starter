package com.wink.autoconfiguration;

import com.wink.autoconfiguration.myaspect.RepeatSubmit;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.expression.MethodBasedEvaluationContext;
import org.springframework.core.DefaultParameterNameDiscoverer;
import org.springframework.core.ParameterNameDiscoverer;
import org.springframework.core.PriorityOrdered;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

/**
 * 我的自动配置类
 *
 * @author xw
 * @ConditionalOnProperty(prefix = "repeatSubmit", name = "enable",havingValue = "true", matchIfMissing = true)
 * 当配置文件有repeatSubmit.repeatSubmitEnable=true时开启，如果配置文件没有设置aspectLog.enable也开启。
 *
 * 通过@ConditionalOnProperty控制配置类是否生效,可以将配置与代码进行分离,实现了更好的控制配置.
 * @ConditionalOnProperty实现是通过havingValue与配置文件中的值对比,返回为true则配置类生效,反之失效.
 *
 * @date 2020-11-24 10:01
 */
@Aspect
@EnableAspectJAutoProxy(exposeProxy = true, proxyTargetClass = true)
@Configuration
@ConditionalOnProperty(prefix = "repeatSubmit", name = "repeatSubmitEnable",
        havingValue = "true",matchIfMissing = true)
@AutoConfigureAfter(RedisAutoConfiguration.class)
public class MyAspectAutoConfiguration implements PriorityOrdered {

    protected Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    /**
     * 解析器
     */
    private static ExpressionParser parser = new SpelExpressionParser();

    /**
     * 名字解析器
     */
    private static ParameterNameDiscoverer nameDiscoverer = new DefaultParameterNameDiscoverer();

    private final static String REPEAT_SUBMIT_KEY = "REPEAT_SUBMIT_KEY:%s#%s#%s#%s";


    @Around("@annotation(com.wink.autoconfiguration.myaspect.RepeatSubmit) ")
    public Object isOpen(ProceedingJoinPoint joinPoint)
            throws Throwable {
        //获取注解
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();
        //目标类、方法、注解参数
        String className = method.getDeclaringClass().getName();
        String methodName = method.getName();
        Object[] args = joinPoint.getArgs();
        RepeatSubmit repeatSubmit = method.getAnnotation(RepeatSubmit.class);

        //获得业务key
        String spelDefinitionKey = getSpelDefinitionKey(repeatSubmit.keys(), method, joinPoint.getArgs());

        String key = String.format(REPEAT_SUBMIT_KEY, spelDefinitionKey, className, methodName, Arrays.toString(args));

        Boolean setnx = redisTemplate.opsForValue().setIfAbsent(key, 1, repeatSubmit.expire(), TimeUnit.SECONDS);
        if (!setnx) {
            throw new RuntimeException(repeatSubmit.message());
        }

        return joinPoint.proceed();
    }

    /**
     * 得到spel表达式参数key
     *
     * @param definitionKey   定义关键
     * @param method          方法
     * @param parameterValues 参数值
     * @return String
     * @author xw
     * @date 2020/4/13 17:36
     */
    private String getSpelDefinitionKey(String definitionKey, Method method, Object[] parameterValues) {
        if (definitionKey != null && !definitionKey.isEmpty()) {
            EvaluationContext context = new MethodBasedEvaluationContext(null, method, parameterValues, nameDiscoverer);
            return Optional.ofNullable(parser.parseExpression(definitionKey).getValue(context)).orElse("null").toString();
        }
        return null;
    }


    @Override
    public int getOrder() {
        //保证事务等切面先执行
        return Integer.MAX_VALUE;
    }

}
