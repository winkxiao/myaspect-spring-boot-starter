# 简单的springboot starter
功能是防止表单重复提交，利用redis实现的


## 快速开始
1. 下载源码到本地
2. 执行mvn install
3. 引入依赖
4. 配置文件开启（默认是开启的）

## 示例
```` java
// 1. maven
<dependency>
      <groupId>com.wink</groupId>
      <artifactId>myaspect-spring-boot-starter</artifactId>
      <version>1.0.7-SNAPSHOT</version>
</dependency>

// 2. 配置文件
// yaml 或 properties 添加（不加也可，默认为开启）
repeatSubmit:
  repeat-submit-enable: true

// 3. 引入使用
@PostMapping("/testStarter")
@RepeatSubmit(expire = 60)
public ApiResponse testStarter() {
    return ResponseUtils.ok();
}
````


